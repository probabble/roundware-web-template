import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
	paper: {
		backgroundColor: '#525252',
	},
}));

export default useStyles;
